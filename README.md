**Display pacman packages information**

**Inspired by** https://wiki.archlinux.org/

**Dependancies**
expac fzf

**Usage**
```
pacinfo {operation} [<package>...]

    -gd, --get-dependencies <package>... Get dependencies of a package(s)
    -gs, --get-size         <package>... Get size of a package(s)
    -gf, --get-files        <package>... Get related files of a package(s)
    -lo, --list-official                 List packages installed by pacman, excluding base and base-devel
    -le, --list-external                 List externally installed packages AUR
    -la, --list-all                      List all installed packages
    -lu, --list-unused                   List unused packages (orphans)
    -ld, --list-details                  List all installed packages with package size and description
    -bi, --browse-installed              Browse all installed packages with fzf
    -ba, --browse-all                    Browse all officially available packages with fzf
    -h,  --help                          Prints help
```
